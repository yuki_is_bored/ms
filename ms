#!/bin/sh

# Copyright (c) 2018 Muhammad Kaisar Arkhan (yuki_is_bored)

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# ms(1) - make site

set -e -f

SOURCE_DIR="src"
TARGET_DIR="out"

[ -n "$1" ] && SOURCE_DIR="$1"
[ -n "$2" ] && TARGET_DIR="$2"

set -u

mkdir -p "$TARGET_DIR"
find "$TARGET_DIR" ! -path "$TARGET_DIR" -prune -exec rm -rf {} +

for folder in \
    $(find "$SOURCE_DIR" \
           ! -path "$SOURCE_DIR" \
           ! -name '.*' \
           ! -name 'static' \
           -type d )
do
    target="$(basename $folder)"
    for file in $(find "$folder" ! -type d | \
                      sort -g)
    do
        cat $file >> "$TARGET_DIR/$target"
    done
done

static_folder="$SOURCE_DIR/static"

[ -d "$static_folder" ] && cp -r "$static_folder" "$TARGET_DIR"
